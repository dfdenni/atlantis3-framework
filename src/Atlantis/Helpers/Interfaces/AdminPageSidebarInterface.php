<?php

namespace Atlantis\Helpers\Interfaces;

interface AdminPageSidebarInterface
{

    public static function getSidebarActionUrl();

    public static function getSidebarName();

}